declare interface IGenderActionWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'GenderActionWebPartStrings' {
  const strings: IGenderActionWebPartStrings;
  export = strings;
}
