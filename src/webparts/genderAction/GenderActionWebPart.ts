import { Version } from '@microsoft/sp-core-library';
import {
  BaseClientSideWebPart,
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-webpart-base';
import { escape } from '@microsoft/sp-lodash-subset';

import styles from './GenderActionWebPart.module.scss';
import * as strings from 'GenderActionWebPartStrings';

import {
  SPHttpClient,
  SPHttpClientResponse   
 } from '@microsoft/sp-http';

export interface IGenderActionWebPartProps {
  description: string;
}

export interface ISPGenderActionItems {
  value: ISPGenderActionItem[];
}

export interface ISPGenderActionItem {
  Title: string;
  Target: string;
  Status: string;
  Archived: boolean;
}

const genderActionListID: string = "3b4f19bb-4053-4802-8872-7b98bdf35f5c";
const genderActionStatuses: string[] = ['Pending','In Progress','Successful', 'Unsuccessful'];

export default class GenderActionWebPart extends BaseClientSideWebPart<IGenderActionWebPartProps> {

  private _listdata: ISPGenderActionItems[] = [];
  private _statusnumber: number[] = [];
  private _statusstyle: Object = {'Pending': 'pending', 'In Progress': 'progress','Successful': 'success', 'Unsuccessful': 'unsuccess'};


  private _renderListInfo(): void {
    let statusCount = 0;
    genderActionStatuses.forEach(function(status) {
      this._getListData(status).then((response) => {
        this._statusnumber[status] = response.value.length;
        this._listdata[status] = response.value;
        //console.log(this._listdata[status]);
        
        // Build circles
        let statusHolder = {'Pending': 'spPending', 'In Progress': 'spProgress','Successful': 'spSuccess', 'Unsuccessful': 'spUnsuccess'};
        this.domElement.querySelector('#'+statusHolder[status]).innerHTML = this._statusnumber[status];

        statusCount++;
        // Show/Hide ListData (action) but only when all for status call are completed        
        if (statusCount == genderActionStatuses.length) this._showHideActionList('Pending');
      });
    }.bind(this));
  }

  // Show/Hide ListData (action)
  private _showHideActionList(status: string):void {
    let html: string = `<h3> &nbsp; ${ status } </h3>`;
    if ((genderActionStatuses.includes(status)) || (typeof this._listdata[status] !== 'undefined')) {
      this._listdata[status].forEach((actionItem: ISPGenderActionItem) => {
        html += `
            <div class="${ styles.actionItem } ${ this._statusstyle[status] }">
              <div class="${ styles.target }"> ${ actionItem.Target } </div>
              ${ actionItem.Title }
            </div>
        `;
      });      
    } else {
      html += `<p>Nothing to display!</p>`;
    }

    const actionList: Element = this.domElement.querySelector('#spActionList');
    actionList.innerHTML = html;
    
  }

  // Change Status Handler
  private _setButtonEventHandlers(): void {
    this.domElement.querySelector('#spPendingBtn').addEventListener('click', () => { this._showHideActionList('Pending'); });
    this.domElement.querySelector('#spProgressBtn').addEventListener('click', () => { this._showHideActionList('In Progress'); });
    this.domElement.querySelector('#spSuccessBtn').addEventListener('click', () => { this._showHideActionList('Successful'); });
    this.domElement.querySelector('#spUnsuccessBtn').addEventListener('click', () => { this._showHideActionList('Unsuccessful'); });
  }


  // Get numbers for each status
  private _getStatusNumber(status:string):Promise<any> {
    return this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl +
      `/_api/web/lists/GetById('${genderActionListID}')/ItemCount?$filter=Status eq '${status}'`, SPHttpClient.configurations.v1).
    then((response:SPHttpClientResponse)=>{
      //console.log(response);
      return response.json();
    }); 
  }

  // Get list data for each status
  private async _getListData(status:string):Promise<ISPGenderActionItems> {
    return this.context.spHttpClient.get(this.context.pageContext.web.absoluteUrl +
      `/_api/web/lists/GetById('${genderActionListID}')/Items?$filter=(Status eq '${status}') and Archived eq 0 &$orderby=Target`, SPHttpClient.configurations.v1).
    then((response:SPHttpClientResponse)=>{
      //console.log(response);
      return response.json();
    }); 
  }

  public render(): void {
    this.domElement.innerHTML = `
      <div class="${ styles.genderAction }">
        <div class="${ styles.container }">          
          <div class="${ styles.circles }" id="spDataCircles">
            <!-- item 1 -->    
            <div class="${ styles.citem }">      
              <div class="${ styles.cpending }">
                <span class="${ styles.number }" id="spPending"> 0 </span>
              </div>
              <div class="${ styles.label }">
                <button type="button" id="spPendingBtn" class="pending"> Pending </button> 
              </div> 
            </div>
            <!-- item 2 -->
            <div class="${ styles.citem }">
              <div class="${ styles.cprogress }">                
                <span class="${ styles.number }" id="spProgress"> 0 </span>
              </div>
              <div class="${ styles.label }">
                <button type="button" id="spProgressBtn" class="progress"> In Progress </button> 
              </div> 
            </div>
            <!-- item 3 -->
            <div class="${ styles.citem }">
              <div class="${ styles.csuccess }">                
                <span class="${ styles.number }" id="spSuccess"> 0 </span>
              </div>
              <div class="${ styles.label }">
                <button type="button" id="spSuccessBtn" class="success"> Successful </button> 
              </div> 
            </div>
            <!-- item 4 -->
            <div class="${ styles.citem }">
              <div class="${ styles.cunsuccess }">                
                <span class="${ styles.number }" id="spUnsuccess"> 0 </span>
              </div>
              <div class="${ styles.label }">
                <button type="button" id="spUnsuccessBtn" class="unsuccess"> Unsuccessful </button> 
              </div> 
            </div>
          </div>
          <div class="${ styles.actionList }" id="spActionList">
            
          </div>
        </div>
      </div>`;

      // Launch
      this._renderListInfo();

      // Event Handlers
      this._setButtonEventHandlers(); 
  }
  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
